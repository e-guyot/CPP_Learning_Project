# Se familiariser avec l'existant

## A- Exécution

Compilez et lancez le programme.

Allez dans le fichier `tower_sim.cpp` et recherchez la fonction responsable de gérer les inputs du programme.
Sur quelle touche faut-il appuyer pour ajouter un avion ? \
$`\textcolor{blue}{\text{Il faut appuyer sur la touche 'c'}}`$ \
Comment faire pour quitter le programme ? \
$`\textcolor{blue}{\text{Il faut appuyer sur la touche 'q' ou 'x'}}`$
A quoi sert la touche 'F' ?
$`\textcolor{blue}{\text{Elle sert à mettre l'écran en fullscreen.}}`$

Ajoutez un avion à la simulation et attendez.
Que est le comportement de l'avion ? \
$`\textcolor{blue}{\text{L'avion fait un tour se pose sur le terminal et redécolle fait un tour etc.}}`$
Quelles informations s'affichent dans la console ? \
$`\textcolor{blue}{\text{Les informations afficher sont lorsque l'avion se fait servir, l'avion décolle, l'avion atteri, l'avion a été servi}}`$

Ajoutez maintenant quatre avions d'un coup dans la simulation.
Que fait chacun des avions ? \
$`\textcolor{blue}{\text{Les trois avions atterissent et le dernier tourne autour de l'aéroport.}}`$

## B- Analyse du code

Listez les classes du programme à la racine du dossier src/.
Pour chacune d'entre elle, expliquez ce qu'elle représente et son rôle dans le programme.

Pour les classes `Tower`, `Aircaft`, `Airport` et `Terminal`, listez leurs fonctions-membre publiques et expliquez précisément à quoi elles servent.
Réalisez ensuite un schéma présentant comment ces différentes classes intéragissent ensemble.

Quelles classes et fonctions sont impliquées dans la génération du chemin d'un avion ?
Quel conteneur de la librairie standard a été choisi pour représenter le chemin ?
Expliquez les intérêts de ce choix.
| $`\textcolor{blue}{\text{Classe}}`$   |    $`\textcolor{blue}{\text{Représentation}}`$        |  $`\textcolor{blue}{\text{Rôle}}`$ |
|----------|:-------------:|------:|
| `aircraft_type` |  $`\textcolor{blue}{\text{Représente les différents types d'avions}}`$ | $`\textcolor{blue}{\text{Permet de stocker les informations propre à un avion}}`$ |
| $`\textcolor{blue}{\text{aircraft}}`$ |    $`\textcolor{blue}{\text{Représente les différents avions ainsi que ses actions}}`$   |   $`\textcolor{blue}{\text{Différencier les avions}}`$ |
| `airport_type` | $`\textcolor{blue}{\text{Représente les différentes informations d'un aéroport}}`$ |    $`\textcolor{blue}{\text{Type d'aéroport}}`$ |
| $`\textcolor{blue}{\text{airport}}`$ | $`\textcolor{blue}{\text{Représente un aéroport}}`$ | 
| $`\textcolor{blue}{\text{geometry}}`$ | $`\textcolor{blue}{\text{Représente les différents mouvements des avions}}`$ | 
$`\textcolor{blue}{\text{main}}`$ | $`\textcolor{blue}{\text{Lancement du projet}}`$ | $`\textcolor{blue}{\text{Permet la visualisation du projet }}`$
| $`\textcolor{blue}{\text{runaway}}`$ | $`\textcolor{blue}{\text{Représente l'avion dans l'espace'}}`$ | $`\textcolor{blue}{\text{Il permet de savoir ou se situe l'avion dans l'espace }}`$
| $`\textcolor{blue}{\text{terminal}}`$ | $`\textcolor{blue}{\text{Représente les différentes terminaux présent}}`$ | $`\textcolor{blue}{\text{Récupère les passagers}}`$
| `tower_sim` | $`\textcolor{blue}{\text{Représente la simulation de la tour}}`$ | $`\textcolor{blue}{\text{Gère les inputs }}`$
| $`\textcolor{blue}{\text{tower}}`$ | $`\textcolor{blue}{\text{Représente les tours de controles}}`$ | 
| $`\textcolor{blue}{\text{waypoint}}`$ | $`\textcolor{blue}{\text{Représente un point sur le chemin d'un avion}}`$ | 


## C- Bidouillons !

1) Déterminez à quel endroit du code sont définies les vitesses maximales et accélération de chaque avion.
Le Concorde est censé pouvoir voler plus vite que les autres avions.
Modifiez le programme pour tenir compte de cela.

2) Identifiez quelle variable contrôle le framerate de la simulation.\
Le framerate correspond au temps de rafraichissement du programme, c'est-à-dire le nombre de fois où les éléments du programme seront mis à jour (ajout de nouvel avion à la simulation, déplacement, etc) en une seconde.\
Ajoutez deux nouveaux inputs au programme permettant d'augmenter ou de diminuer cette valeur.
Essayez maintenant de mettre en pause le programme en manipulant ce framerate. Que se passe-t-il ?\
$`\textcolor{blue}{\text{Si le framerate est à 0 le programme crash }}`$
Ajoutez une nouvelle fonctionnalité au programme pour mettre le programme en pause, et qui ne passe pas par le framerate.

3) Identifiez quelle variable contrôle le temps de débarquement des avions et doublez-le.
$`\textcolor{blue}{\text{C'est la variable}}`$ `SERVICE_CYCLES`

4) Lorsqu'un avion a décollé, il réattérit peu de temps après.
Assurez-vous qu'à la place, il soit supprimé de la `move_queue`.\
Pour tester, il suffit de dézoomer et de vérifier que les avions suffisament éloignés ne bougent plus.
Indices :\
A quel endroit pouvez-vous savoir que l'avion doit être supprimé ?\
$`\textcolor{blue}{\text{On peut le savoir dans}}`$ `tower::get_instruction()`
Pourquoi n'est-il pas sûr de procéder au retrait de l'avion dans cette fonction ? \
$`\textcolor{blue}{\text{Un autre objet peut avoir un pointeur dessus.}}`$
A quel endroit de la callstack pourriez-vous le faire à la place ?\
$`\textcolor{blue}{\text{Dans la fonction move de }}`$ `opengl_interface`\
Que devez-vous modifier pour transmettre l'information de la première à la seconde fonction ?\
$`\textcolor{blue}{\text{Soit on modifie le retour de la fonction, soit creer une nouvelle fonction pour savoir si l'objet est supprimer.}}`$

5) Lorsqu'un objet de type `Displayable` est créé, il faut ajouter celui-ci manuellement dans la liste des objets à afficher.
Il faut également penser à le supprimer de cette liste avant de le détruire.
Faites en sorte que l'ajout et la suppression de `display_queue` soit "automatiquement gérée" lorsqu'un `Displayable` est créé ou détruit.\
Essayez maintenant de supprimer complètement l'avion du programme lorsque vous le retirez de la `move_queue`.\
En dézoomant, vous devriez maintenant constater que les avions disparaissent maintenant de l'écran.

6) La tour de contrôle a besoin de stocker pour tout `Aircraft` le `Terminal` qui lui est actuellement attribué, afin de pouvoir le libérer une fois que l'avion décolle.
Cette information est actuellement enregistrée dans un `std::vector<std::pair<const Aircraft*, size_t>>` (size_t représentant l'indice du terminal).
Cela fait que la recherche du terminal associé à un avion est réalisée en temps linéaire, par rapport au nombre total de terminaux.
Cela n'est pas grave tant que ce nombre est petit, mais pour préparer l'avenir, on aimerait bien remplacer le vector par un conteneur qui garantira des opérations efficaces, même s'il y a beaucoup de terminaux.\
Modifiez le code afin d'utiliser un conteneur STL plus adapté. Normalement, à la fin, la fonction `find_craft_and_terminal(const Aicraft&)` ne devrait plus être nécessaire.

## D- Théorie

1) Comment a-t-on fait pour que seule la classe `Tower` puisse réserver un terminal de l'aéroport ?
$`\textcolor{blue}{\text{Tower est déclarer comme friend de l'aéroport et la fonction}}`$ `reserve_terminal` $`\textcolor{blue}{\text{est private dans airport.}}`$

2) En regardant le contenu de la fonction `void Aircraft::turn(Point3D direction)`, pourquoi selon-vous ne sommes-nous pas passer par une réference constante ?
$`\textcolor{blue}{\text{On peut pas mettre une référence constante car on utilise}}`$ `cap_lenght()` $`\textcolor{blue}{\text{qui modifie l'objet.}}`$
Pourquoi n'est-il pas possible d'éviter la copie du `Point3D` passé en paramètre ?
$`\textcolor{blue}{\text{On est obliger de passer par copie pour avoir le même Point3D au passage de cette fonction si on effectue plusieurs fois cette fonction.}}`$

## E- Bonus

Le temps qui s'écoule dans la simulation dépend du framerate du programme.
La fonction move() n'utilise pas le vrai temps. Faites en sorte que si.
Par conséquent, lorsque vous augmentez le framerate, la simulation s'exécute plus rapidement, et si vous le diminuez, celle-ci s'exécute plus lentement.

Dans la plupart des jeux ou logiciels que vous utilisez, lorsque le framerate diminue, vous ne le ressentez quasiment pas (en tout cas, tant que celui-ci ne diminue pas trop).
Pour avoir ce type de résultat, les fonctions d'update prennent généralement en paramètre le temps qui s'est écoulé depuis la dernière frame, et l'utilise pour calculer le mouvement des entités.

Recherchez sur Internet comment obtenir le temps courant en C++ et arrangez-vous pour calculer le dt (delta time) qui s'écoule entre deux frames.
Lorsque le programme tourne bien, celui-ci devrait être quasiment égale à 1/framerate.
Cependant, si le programme se met à ramer et que la callback de glutTimerFunc est appelée en retard (oui oui, c'est possible), alors votre dt devrait être supérieur à 1/framerate.

Passez ensuite cette valeur à la fonction `move` des `DynamicObject`, et utilisez-la pour calculer les nouvelles positions de chaque avion.
Vérifiez maintenant en exécutant le programme que, lorsque augmentez le framerate du programme, vous n'augmentez pas la vitesse de la simulation.

Ajoutez ensuite deux nouveaux inputs permettant d'accélérer ou de ralentir la simulation.

```diff
| BLUE text
```
