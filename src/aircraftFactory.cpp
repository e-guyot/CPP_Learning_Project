
#include "aircraftFactory.hpp"

#include "aircraft.hpp"
#include "aircraft_types.hpp"
#include "tower.hpp"

#include <memory>

std::unique_ptr<Aircraft> AircraftFactory::create_aircraft(const AircraftType& type, Tower& tower)
{

    std::string number = airlines[std::rand() % 8] + std::to_string(1000 + (rand() % 9000));
    while(aircraft_number_flight.find(number) != aircraft_number_flight.end()){
        number = airlines[std::rand() % 8] + std::to_string(1000 + (rand() % 9000));
    }
    const std::string flight_number = number;
    aircraft_number_flight.emplace(flight_number);
    const float angle       = (rand() % 1000) * 2 * 3.141592f / 1000.f; // random angle between 0 and 2pi
    const Point3D start     = Point3D { std::sin(angle), std::cos(angle), 0 } * 3 + Point3D { 0, 0, 2 };
    const Point3D direction = (-start).normalize();
    int fuel = rand() % (3000-150) + 150;

    return std::make_unique<Aircraft>(type, flight_number, start, direction, tower, fuel);
}

std::unique_ptr<Aircraft> AircraftFactory::create_random_aircraft(Tower& tower)
{
    return create_aircraft((aircraft_types[rand() % 3]), tower);
}