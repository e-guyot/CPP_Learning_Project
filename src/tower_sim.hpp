#pragma once
#include "aircraftFactory.hpp"
#include "aircraftManager.hpp"

class Airport;
struct AircraftType;
/* TASK 1 : Obj 2A  */
struct ContextInitializer
{
    ContextInitializer(int argc, char** argv)
    {
        MediaPath::initialize(argv[0]);                            /* init les images */
        std::srand(static_cast<unsigned int>(std::time(nullptr))); /* init les random */
        GL::init_gl(argc, argv, "Airport Tower Simulation");       /* init opengl */
    }
};

class TowerSimulation
{
private:
    bool help        = false;
    Airport* airport = nullptr;
    AircraftManager aircraft_manager;
    /* TASK 1 : Obj 2A  */
    ContextInitializer context_initializer;
    AircraftFactory aircraft_factory;

    TowerSimulation(const TowerSimulation&) = delete;
    TowerSimulation& operator=(const TowerSimulation&) = delete;

    void create_keystrokes();
    void display_help() const;

    void init_airport();

public:
    TowerSimulation(int argc, char** argv);
    ~TowerSimulation();

    void launch();
};
