#include "aircraftManager.hpp"

#include "aircraft.hpp"

#include <memory>
#include <utility>
#include <algorithm>

void AircraftManager::move()
{
    // sorted enfonction de qui a un terminal en premier puis du fuel
    std::sort(aircrafts.begin(), aircrafts.end(),
              [](std::unique_ptr<Aircraft>& a, std::unique_ptr<Aircraft>& a2)
              {
                  if (a->has_terminal() && !a2->has_terminal())
                  {
                      return true;
                  }
                  else if (!a->has_terminal() && a2->has_terminal())
                  {
                      return false;
                  }
                  return a->fuel < a2->fuel;
              });
    aircrafts.erase(std::remove_if(aircrafts.begin(), aircrafts.end(),
                                   [this](std::unique_ptr<Aircraft>& a)
                                   {
                                       try
                                       {
                                           a->move();
                                           return a->toDelete();
                                       } catch (AircraftCrash& airError)
                                       {
                                           nbCrash++;
                                           std::cerr << "erreur de crash" << std::endl;
                                           return true;
                                       }
                                   }),
                    aircrafts.end());
}

void AircraftManager::add(std::unique_ptr<Aircraft> aircraft)
{
    aircrafts.emplace_back(std::move(aircraft));
}

int AircraftManager::get_required_fuel() const
{
    return std::accumulate(aircrafts.begin(), aircrafts.end(), 0,
                           [](int x, const std::unique_ptr<Aircraft>& a)
                           {
                               if (a->is_low_on_fuel() && a->is_served)
                               {
                                   return x + (3000 - a->fuel);
                               }
                               return x;
                           });
}

int AircraftManager::sumAircraft(std::string airline) const
{
    return std::count_if(aircrafts.begin(), aircrafts.end(),
                         [airline](const std::unique_ptr<Aircraft>& aircraft)
                         { return aircraft->get_flight_num().rfind(airline, 0) == 0; });
}