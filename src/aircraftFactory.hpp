#pragma once

#include "aircraft_types.hpp"

class Aircraft;
class Tower;
class AircraftFactory
{
private:
/* TASK1 : Obj 2.A */
    std::array<AircraftType, 3> aircraft_types {AircraftType{ .02f, .05f, .02f, MediaPath { "l1011_48px.png" } }, AircraftType{ .02f, .05f, .02f, MediaPath { "b707_jat.png" } },AircraftType{ .02f, .07f, .02f, MediaPath { "concorde_af.png" } }}  ;
    std::string airlines[8]                       { "AF", "LH", "EY", "DL", "KL", "BA", "AY", "EY" };
    std::set<std::string> aircraft_number_flight{};

public:
    std::unique_ptr<Aircraft> create_aircraft(const AircraftType& type, Tower& tower);
    std::unique_ptr<Aircraft> create_random_aircraft(Tower& tower);
    std::string getAirlines(int num) const { assert(num < 8 && num >= 0); return airlines[num]; }

};