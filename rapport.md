# Rapport du projet

Ce rapport contient : 

- les réponses aux questions ‘non-code’ dans les différentes tâches, si vous n’y avez pas répondu directement dans les fichiers TASK_#.md. Ces réponses ont été directement mis dans les différents fichiers. 
- les choix auxquels vous avez été confrontés par rapport à l’architecture du programme, et les raisons qui ont fait que vous êtes parti dans une direction plutôt qu’une autre,
- les situations où vous avez bloqué, et si applicable, la solution que vous avez trouvée pour vous en sortir,
- ce que vous avez aimé et/ou détesté dans ce projet,
- ce que vous pensez en avoir appris.

## Les choix 
J'ai ajouté une fonction toDelete() dans Aircraft et c'est elle qui m'indique quand je vais supprimer l'avion alors que j'aurais pu faire que la fonction move (ou update) renvoie un boolean à la place, en faisant ainsi je garde la compatibilité ascendante tout le long de mon développement.

## Les situations bloquantes 
Bien comprendre ce qui est demandé dans les différentes tâches.
Je n'ai pas très bien compris comment utiliser les templates.  

## La critique sur ce projet
Les plus dans la réalisation de ce type de projet : 
- travailler comme en entreprise : 
    - partir sur de l'existant
    - l'utilisation de git

Les moins : 
- Difficulté à s'approprier le code
- N'importe qui a accès aux projets des autres

## Ce que j'ai appris 

Ce projet m'a permis d'apprendre à coder sur une base de programmation non web. Et de voir comment le C++ est complexe surtout au niveau du typage des variables.  
